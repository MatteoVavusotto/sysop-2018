/*
Il modulo dimezza.c implementa una funzione che prende un argomento double e
restituisce un double pari alla meta’ del COSENO dell’argomento passato. man 3 cos
spiega come usare coseno e come linkare libreria. Prima di restituire il risultato, la
funzione copia il risultato in una variabile double globale nel modulo dimezza.c, variabile
chiamata salva.
*/

#include "dimezza.h"
#include <math.h>

double salva;

double dimezza(double n) {
    salva = cos(n) / 2.0;
    return salva;
}
