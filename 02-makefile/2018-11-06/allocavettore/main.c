#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

int main(int argc, char **argv) {
    int32_t * ptr;
    int i;

    ALLOCAVETT(ptr);

    if (ptr == NULL) {
        return 1;
    }
    
    for (i = 0; i < 10; i++) {
        ptr[i] = -19 + i;
    }

    return 0;
}
