#!/bin/bash

find /usr -maxdepth 2 -type f -iname '*i.h' -exec echo -n "{}: " \; -exec bash -c 'wc -l "{}" | cut -d" " -f1' \;
