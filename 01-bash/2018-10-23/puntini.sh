#!/bin/bash

if [[ $1 -lt 0 ]]; then exit 1; fi

NUM=0
while (( $NUM <= $1 )); do
	echo -n " .${BASHPID}"
	sleep 1
	((NUM+=1))
done
